import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as adminComponents from './components';



const routes: Routes = [
  {
    path: 'country',
    component : adminComponents.CountryComponent
  },
  {
    path: 'edit-country/:countryID',
    component : adminComponents.EditCountryComponent
  },
  {
    path: 'delete-country/:countryID',
    component : adminComponents.DeleteCountryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
