import { Component, OnInit } from '@angular/core';
import { post } from '../../models';
import { urls } from './../../../@core/helpers/urls/urls';

import { httpService } from '../../../@core'
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  listOfPosts: post[];

  constructor(private httpService: httpService, private route: Router) { }

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts(){
    let postURL = `${urls.LOAD_OFFER}`;
    this.httpService.Get(postURL).subscribe((response: post[]) => {
      this.listOfPosts = response;
    })
  }

  editCountry(userId){
    this.route.navigate(['/admin/edit-country' , userId]);
  }

  deleteCountry(userId){
    this.route.navigate(['/admin/delete-country' , userId]);
  }
}
