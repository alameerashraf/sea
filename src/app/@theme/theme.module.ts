import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NB_MODULES, NB_MODULE , NB_THEMES } from './theme.modeule.imports';
import * as themeComponents from './components';
import * as themePipes from './pipes';
import * as themeLayout from './layouts';

import { objectTransfer, httpService } from './../@core';



@NgModule({
  imports: [
  CommonModule,
    ...NB_MODULES,
  ],
  exports: [
    CommonModule,
    themePipes.CapitalizePipe,
    themePipes.NumberWithCommasPipe,
    themePipes.PluralPipe,
    themePipes.RoundPipe,
    themePipes.TimingPipe,
    themeComponents.FooterComponent,
    themeComponents.HeaderComponent,
    themeComponents.SearchInputComponent,
    themeLayout.MainLayoutComponent,
    themeLayout.OneColumnLayoutComponent,
    themeComponents.ModalBodyComponent,
    themeComponents.WedgitCardComponent,
    themeComponents.VerticalStepperComponent,
    themeComponents.AnimatedCountComponent,
   ],
  declarations: [
    themePipes.CapitalizePipe,
    themePipes.NumberWithCommasPipe,
    themePipes.PluralPipe,
    themePipes.RoundPipe,
    themePipes.TimingPipe,
    themeComponents.FooterComponent,
    themeComponents.HeaderComponent,
    themeComponents.SearchInputComponent,
    themeLayout.MainLayoutComponent,
    themeLayout.OneColumnLayoutComponent,
    themeComponents.ModalBodyComponent,
    themeComponents.WedgitCardComponent,
    themeComponents.VerticalStepperComponent,
    themeComponents.AnimatedCountComponent
  ],
  entryComponents: [
    themeComponents.ModalBodyComponent,
  ],
  providers: [
    objectTransfer,
    httpService
  ]
})
export class ThemeModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ThemeModule,
      providers: [
        ...NB_MODULE.forRoot(
          {
            name: 'default',
          },
          [ ...NB_THEMES ],
        ).providers,
      ],
    };
  }
}
