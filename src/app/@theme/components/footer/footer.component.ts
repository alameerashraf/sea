import { Component } from '@angular/core';
import { NbMenuService } from '@nebular/theme';
import { httpService, urls } from '../../../@core';

@Component({
  selector: 'seqt-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
    <b><a href="" (click)="navigateHome()" target="_blank"> Schneider Electric Assets | SEA </a> &nbsp;</b>{{ year }}</span>
    <span class="float-right"> 
      <label style="font-weight: 700;color: #05D890;margin-bottom: 0px !important;" >{{ env }}</label> &nbsp;
      <label style="font-weight: 700;color: #05D890;margin-bottom: 0px !important;"> v {{ version }} </label>
      </span>
  `,
})
export class FooterComponent {
  year = new Date().getFullYear();
  version = "";
  env: "";
  constructor(private menuService: NbMenuService, 
    private httpService: httpService ){
      this.getCurrentEnv();
    
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  getCurrentEnv(){
    let utility = `${urls.SERVER_URL}${urls.UTILITY}`;
    this.httpService.Get(utility , {}).subscribe((utility: any) => {
      this.version = utility.version;
      this.env = utility.env;
    });
  }
}
