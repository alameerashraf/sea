import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';


import { COMPONENTS , NBULAR_THEME } from './app.imports';

import {
  AppConfig, 
  httpService, 
  localStorageService,
  authorizationService, 
  headerInterceptor
} from './@core';
import { PrivacyComponent } from './app-moduel-components/privacy/privacy.component';


@NgModule({
  declarations: [
    COMPONENTS.AppComponent,
    COMPONENTS.AppStarterComponent,
    COMPONENTS.AppLandingComponent,
    COMPONENTS.PrivacyComponent,
    PrivacyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NBULAR_THEME.ThemeModule.forRoot(),
    NBULAR_THEME.NbSidebarModule.forRoot(),
    NBULAR_THEME.NbMenuModule.forRoot(),
    NBULAR_THEME.NbDatepickerModule.forRoot(),
    NBULAR_THEME.NbDialogModule.forRoot(),
    NBULAR_THEME.NbWindowModule.forRoot(),
    NBULAR_THEME.NbToastrModule.forRoot(),
    NBULAR_THEME.CoreModule.forRoot()
  ],
  bootstrap: [
    COMPONENTS.AppComponent
  ],
  providers: [
    authorizationService,
    httpService,
    localStorageService,
    AppConfig,
    { 
      provide: APP_INITIALIZER , 
      useFactory: (config: AppConfig)=> () => config.load() , 
      deps: [AppConfig] , 
      multi: true 
    },
    { provide: HTTP_INTERCEPTORS , useClass : headerInterceptor , multi : true  },
  ]
})
export class AppModule {
}
