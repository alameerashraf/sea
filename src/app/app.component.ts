import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/theme';
import { Router, NavigationStart } from '@angular/router';
import { localStorageService } from './@core/services/local-storage/local-storage';

@Component({
  selector: 'seqt-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService) {}

  ngOnInit(): void {
    this.analytics.trackPageViews();
  }
}
