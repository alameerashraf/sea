import { AppConfig } from '../../config';
import { urls } from '../urls';

export class constants {
  public static _config: AppConfig;

  public static get ROLES() {
    enum ROLES { 
      "TENDERING_ENGINEER" = "TenderingEngineer",
      "TENDEDRING_MANAGER" = "TenderingManager",
      "SALES_MANAGER" = "SalesManager",
      "SALES_ENGINEER" = "SalesEngineer",
      "PROJECT_MANAGER" = "ProjectManager",
      "TOOL_ADMIN" = "ToolAdmin",
      "FINANCIAL_ADMIN" = "FinancialAdmin",
      "MARKETING_ADMIN" = "MarketingAdmin",
      "SUPER_ADMIN" = "SuperAdmin"
    };
    
    return ROLES;
  };

  public static get MENU_ITEMS_PER_ROLE(){
    let items = {
      "SuperAdmin" : [
        'ADMIN_SPLITTER',
        'ADMIN',
      ],
      "TenderingManager" : [
        "TENDERING_DASHBOARD",
        "OFFERS_SPLITTER",
        "CREATE_NEW_OFFER",
      ],
      "TenderingEngineer" : [
        "TENDERING_DASHBOARD",
      ],
      "SalesManager" : [
        "SALES_DASHBOARD"
      ],
      "SalesEngineer" : [
        "SALES_DASHBOARD"
      ],
      "ProjectManager" : [
        "PM_DASHBOARD"
      ],
      "PRIVACY": [
        'PRIVACY_SPLITTER',
        'PRIVACY_LINK'
      ]
    }
    return items;
  };




  public static get SERVER_ERROR(): string { return  this._config.getConst("SERVER_ERROR_MESSAGE")};
  public static get BFO_LOADING_ERROR(): string { return  this._config.getConst("BFO_LOADING_ERROR")};
  public static get SUCCESSFULL_SAVING(): string { return  this._config.getConst("SUCCESSFULL_SAVING")};
  public static get FAILURE_SAVING(): string { return  this._config.getConst("FAILURE_SAVING")};
  public static get LOGGED_IN_USER(): string { return  this._config.getConst("LOGGED_IN_USER")};
  public static get LOGGED_IN_USER_ROLES(): string { return  this._config.getConst("LOGGED_IN_USER_ROLES")};
}
