export class log {
  public static log(message, level, locationOfMessage, whereToLog) {
    switch (whereToLog) {
      case "server":
        this.inServerLogging(message, level, locationOfMessage);
        break;
      case "inApp":
        this.inAppLogging(message, level, locationOfMessage);
        break;
      case "both": {
        this.inServerLogging(message, level, locationOfMessage);
        this.inAppLogging(message, level, locationOfMessage);
      }
      default:
        this.inServerLogging(message, level, locationOfMessage);
    }
  }

  private static inAppLogging(message, level, locationOfMessage) {
    var messageToLog = `Message:  ${message}  ## In:  ${locationOfMessage}  ##  timestamp:   ${new Date().toLocaleString()}`;
    switch (level) {
      case "error":
        console.error(messageToLog);
        break;
      case "info":
        console.info(messageToLog);
        break;
      case "warn":
        console.warn(messageToLog);
        break;
      case "debug":
        console.log(messageToLog);
        break;
    }
  }

  private static inServerLogging(message, level, locationOfMessage) {
    //TODO: logging this in a db collection in db, sent rquest to the API.
    console.log("SERVER LOGGING IN DB");
  }
}
