import { FormGroup } from "@angular/forms";


export function matchThreshold(controls: string[]) {
  return (fg: FormGroup) => {

    let count = 0;

    controls.forEach(controlName => {
      const controlValue = fg.controls[controlName].value;
      let value = isNaN(parseInt(controlValue)) ? 0 : parseInt(controlValue);
      count = count + value;
    });

    if (count != 100) {
      controls.forEach(controlName => {
        const control = fg.controls[controlName];
        control.setErrors({ matchThreshold: true });
      });
    } else {

      controls.forEach(controlName => {
        const control = fg.controls[controlName];
        control.setErrors(null);
      });
    }
  };
}
