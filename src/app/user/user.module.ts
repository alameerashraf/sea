import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';

import { MAT_MODAL , NgxMask , NBULAR_THEME , NgbModules} from './user.imports';

import {
  httpService,
  localStorageService,
} from '../@core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ThemeModule } from '../@theme/theme.module';

@NgModule({
  declarations: [
  ],
  imports: [
    MAT_MODAL,
    ThemeModule,
    NgxMask.forRoot(),
    ...NBULAR_THEME,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UserRoutingModule,
    NgbModules,
  ],
  providers: [
    httpService,
    localStorageService,
  ],
})
export class UserModule { }
