import {
    NbTooltipModule,
    NbAccordionModule,
    NbListModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbTabsetModule, 
    NbInputModule,
    NbDatepickerModule,
    NbActionsModule,
    NbLayoutModule,
    NbMenuModule,
    NbSearchModule,
    NbSidebarModule,
    NbThemeModule,
    NbContextMenuModule,
    NbUserModule,
    NbButtonModule,
    NbSelectModule,
    NbIconModule,
    NbCardModule,
    NbDialogModule,
    NbTreeGridModule,
    NbRadioModule,
    NbCheckboxModule,
    NbSpinnerModule
} from '@nebular/theme';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbSecurityModule } from '@nebular/security';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { NgxMaskModule } from 'ngx-mask';

import { MatDialogModule } from '@angular/material'

export const NBULAR_THEME = [
    NbSecurityModule,
    NbAccordionModule,
    NbButtonModule,
    NbCardModule,
    NbListModule,
    NbRouteTabsetModule,
    NbStepperModule,
    NbTabsetModule, 
    NbUserModule,
    NbInputModule,
    NbSelectModule,
    NbDatepickerModule,
    NbIconModule,
    NbDialogModule,
    NbActionsModule,
    NbLayoutModule,
    NbMenuModule,
    NbSearchModule,
    NbSidebarModule,
    NbThemeModule,
    NbContextMenuModule,
    Ng2SmartTableModule,
    NbTreeGridModule,
    NbTooltipModule,
    NbRadioModule,
    NbCheckboxModule,
    NbSpinnerModule
];

export const MAT_MODAL = MatDialogModule;

export const NgxMask = NgxMaskModule;

export const NgbModules = NgbModule;
