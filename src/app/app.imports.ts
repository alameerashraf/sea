import { AppComponent } from './app.component';
import { AppLandingComponent } from './app-landing.component';
import { AppStarterComponent } from './app-starter.component';
import { PrivacyComponent }  from './app-moduel-components/privacy/privacy.component';

import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';

import {
    NbChatModule,
    NbDatepickerModule,
    NbDialogModule,
    NbMenuModule,
    NbSidebarModule,
    NbToastrModule,
    NbWindowModule,
  } from '@nebular/theme';


export const COMPONENTS = {
    AppComponent,
    AppLandingComponent,
    AppStarterComponent,
    PrivacyComponent
};

export const NBULAR_THEME = {
    NbChatModule,
    NbDatepickerModule,
    NbDialogModule,
    NbMenuModule,
    NbSidebarModule,
    NbToastrModule,
    NbWindowModule,
    CoreModule,
    ThemeModule
};


